<?php declare(strict_types=1);

namespace Persist\LoggerBundle\Logger;

use Assert\Assertion;
use Psr\Log\LogLevel;
use Psr\Log\AbstractLogger;
use Psr\Log\LoggerInterface;
use Assert\AssertionFailedException;

class SentryLogger extends AbstractLogger implements LoggerInterface
{
    /** @var \Raven_Client */
    private $client;

    /** @var array */
    private $logLevelMap = [
        LogLevel::ALERT => LogLevel::ERROR,
        LogLevel::CRITICAL => LogLevel::ERROR,
    ];

    /**
     * @param \Raven_Client $client
     */
    public function __construct(\Raven_Client $client)
    {
        $this->client = $client;
        $this->configureClient();
    }

    /**
     * @return void
     */
    private function configureClient(): void
    {
        $this->client->store_errors_for_bulk_send = true;
    }

    /**
     * @inheritDoc
     */
    public function log($level, $message, array $context = [])
    {
        $this->validate($level, $message, $context);
        $level = $this->mapLogLevel($level);

        if ($message instanceof ExceptionMessage) {
            $this->client->captureException($message->getException(), $this->buildData($level, $message));
        } elseif ($message instanceof Message) {
            $this->client->captureMessage($message->getMessage(), [], $this->buildData($level, $message));
        } else {
            throw new \LogicException('Unsupported Message object');
        }
    }

    /**
     * @param string $level
     *
     * @return string
     */
    private function mapLogLevel(string $level): string
    {
        return $this->logLevelMap[$level] ?? $level;
    }

    /**
     * @param mixed $level
     * @param mixed $message
     * @param array $context
     *
     * @throws AssertionFailedException
     */
    private function validate($level, $message, array $context): void
    {
        Assertion::string($level);
        Assertion::isInstanceOf($message, Message::class);
        Assertion::noContent($context);
    }

    /**
     * @param string $level
     * @param Message $message
     *
     * @return array
     */
    private function buildData(string $level, Message $message): array
    {
        return [
            'level' => $level,
            'tags' => $message->getTags(),
            'extra' => $message->getExtra(),
        ];
    }
}
