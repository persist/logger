<?php declare(strict_types=1);

namespace Persist\LoggerBundle\Logger;

class Enricher
{
    /**
     * @param array $record
     *
     * @return array
     */
    public function __invoke(array $record): array
    {
        $this->ensureFieldTypes($record);
        $this->addExceptionClass($record);

        return $record;
    }

    /**
     * @param array $record
     */
    private function addExceptionClass(array &$record): void
    {
        $exception = $record['context']['exception'] ?? null;

        if ( ! $exception instanceof \Throwable) {
            return;
        }

        $record['extra']['exception_class'] = get_class($exception);
    }

    /**
     * @param array $record
     */
    private function ensureFieldTypes(array &$record): void
    {
        if ( ! isset($record['context']) || ! is_array($record['context'])) {
            return;
        }

        foreach ($record['context'] as $key => &$field) {
            if ( ! is_string($key)) {
                unset($record['context'][$key]);

                continue;
            }

            if (strpos($key, 'tag_') === 0) {
                $field = (string)$field;
            } elseif (strpos($key, 'count_') === 0) {
                $field = (int)$field;
            } elseif (strpos($key, 'duration_') === 0) {
                $field = (float)$field;
            }
        }
    }
}
