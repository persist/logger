<?php declare(strict_types=1);

namespace Persist\LoggerBundle\Logger;

use Sentry\SentryBundle\SentrySymfonyClient;

class SymfonySentryLogger extends SentrySymfonyClient
{
    /**
     * @inheritdoc
     */
    public function __construct(?string $dsn = null, array $options = [])
    {
        parent::__construct($this->isVagrantEnvironment() ? null : $dsn, $options);
    }

    /**
     * @return bool
     */
    protected function isVagrantEnvironment(): bool
    {
        return getenv('HOME') === '/home/vagrant' || getenv('VAGRANT') === 'VAGRANT';
    }
}
