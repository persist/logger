<?php declare(strict_types=1);

namespace Persist\LoggerBundle\Logger;

class Message
{
    /** @var string */
    protected $message;

    /** @var array */
    protected $tags = [];

    /** @var array */
    protected $extra = [];

    /**
     * @param string $message
     * @param array $tags
     * @param array $extra
     */
    public function __construct(string $message, array $tags = [], array $extra = [])
    {
        $this->message = $message;
        $this->tags = $tags;
        $this->extra = $extra;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @return array
     */
    public function getTags(): array
    {
        return $this->tags;
    }

    /**
     * @return array
     */
    public function getExtra(): array
    {
        return $this->extra;
    }
}
