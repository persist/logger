<?php declare(strict_types=1);

namespace Persist\LoggerBundle\Logger;

class ExceptionMessage extends Message
{
    /** @var \Throwable */
    private $exception;

    /**
     * @inheritDoc
     */
    public function __construct(\Throwable $exception, array $tags = [], array $extra = [])
    {
        $this->exception = $exception;
        parent::__construct('', $tags, $extra);
    }

    /**
     * @return \Throwable
     */
    public function getException(): \Throwable
    {
        return $this->exception;
    }
}
