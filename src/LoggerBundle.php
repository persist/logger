<?php declare(strict_types=1);

namespace Persist\LoggerBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class LoggerBundle extends Bundle
{
}
